import os
import uiMdl
reload(uiMdl)
import scnMdl
reload(scnMdl)
import pipeMdl
reload(pipeMdl)
import toolsMdl
reload(toolsMdl)
import opnUI
reload(opnUI)
import verUpUI
reload(verUpUI)
import pubUI
reload(pubUI)
import astLdrUI
reload(astLdrUI)
import configSveUI
reload(configSveUI)
import configLdrUI
reload(configLdrUI)
import lookPubUI
reload (lookPubUI)
import assetToolsUI
reload(assetToolsUI)
import renderToolsUI
reload(renderToolsUI)

def open(scene):
    cmds.file(scene, open = True, force = True, prompt = True)

def openCheck(path,list): 
    badAssets = []
    if os.path.exists(path):
        assets = os.listdir(path)
        for asset in sorted(assets):
            if asset not in list:
                pathToVer = path +'/' + asset + '/Maya/scenes/Modeling/Versions/'
                if os.path.exists(pathToVer):
                    versions = os.listdir(pathToVer)
                    highVer = max(versions).replace("v","")
                    scene = pathToVer + 'v' + highVer + '/Modeling.ma'
                    #print 'opening asset ' + asset
                    try:
                        open(scene)
                        test = True
                        try:
                            if pipeMdl.checkComply() == False:
                                test = False
                            if pipeMdl.checkNames() == False:
                                test = False
                            if pipeMdl.checkConstraints() == False:
                                test = False
                            if test==False:
                                badAssets.append(asset)
                        except:
                            badAssets.append(asset)
                    except:
                        badAssets.append(asset)
                else:
                    pass
                    #print asset + ' does not exist'
                    badAssets.append(asset)
    return badAssets 


def openRename(path,list): 
    badAssets = []
    if os.path.exists(path):
        assets = os.listdir(path)
        for asset in sorted(assets):
            if asset in list:
                pathToVer = path +'/' + asset + '/Maya/scenes/Modeling/Versions/'
                if os.path.exists(pathToVer):
                    versions = os.listdir(pathToVer)
                    highVer = max(versions).replace("v","")
                    scene = pathToVer + 'v' + highVer + '/Modeling.ma'
                    #print 'opening asset ' + asset
                    try:
                        open(scene)
                        test = True
                        try:
                            if pipeMdl.checkComply() == False:
                                test = False
                            if pipeMdl.checkNames() == False:
                                renameDuplicates()
                                renameDuplicates()
                                renameDuplicates()
                                renameDuplicates()
                                renameDuplicates()
                                renameDuplicates()
                                renameDuplicates()
                                cmds.file( save=True, type='mayaAscii')
                            if pipeMdl.checkConstraints() == False:
                                test = False
                            if test==False:
                                badAssets.append(asset)
                        except:
                            badAssets.append(asset)
                    except:
                        badAssets.append(asset)
                else:
                    pass
                    #print asset + ' does not exist'
                    badAssets.append(asset)
    return badAssets        


list = ['14', '25', '32', '33', '37', '38', '39', '40', '41', '59', '79', '84', '86', '87', '90','121', '122', '127', '128', '129', '142', '143', '144', '145', '146', '149', '155', '166', '168', '169', '171', '173', '174', '175', '176', '177', '178', '179', '184', '185', '196', '198', '199', '209', '212', '213', '216', '218', '219', '220', '221', '228', '229', '230', '249', '251', '263', '264']
path = 'I:/NAVISTAR_ASSET_REPOSITORY_TEST'

def openCheck(path,list): 
    badAssets = []
    if os.path.exists(path):
        assets = os.listdir(path)
        for asset in sorted(assets):
            if asset in list:
                pathToVer = path +'/' + asset + '/Maya/scenes/Modeling/Versions/'
                if os.path.exists(pathToVer):
                    versions = os.listdir(pathToVer)
                    highVer = max(versions).replace("v","")
                    scene = pathToVer + 'v' + highVer + '/Modeling.ma'
                    #print 'opening asset ' + asset
                    try:
                        open(scene)
                        print 'opening' + asset  
                        try:
                            p = pfd.PullFromDatabase()
                            p.setup()
                            p.cleanup()
                            p.createGroups()
                            p.createControls()
                            p.createHooks()
                            p.constrainSingleHooks()
                            p.createAnchors()
                            pipeMdl.assetPublish()
                        except:
                            badAssets.append(asset)
                    except:
                        badAssets.append(asset)
                else:
                    pass
                    #print asset + ' does not exist'
                    badAssets.append(asset)
    return badAssets 


print openCheck(path,list)