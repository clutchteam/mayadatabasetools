# python

import os
import sys
import maya.cmds as cmds

class PullFromDatabase():
    def setup(self):
        # ===========================================================
        # get asset ID from file path
        # ===========================================================
        paths = sys.path
        path = str(cmds.file(query = True, list = True)[0])
        assetID = path[path.index('/') + 1:]
        assetID = assetID[assetID.index('/') + 1:]
        assetID = int(assetID[:assetID.index('/')])
        print 'Asset ID derived from scene path: ' + str(assetID)

        # ===========================================================
        # load assetDB python module and open test database
        # ===========================================================
        pipePath = 'N:/Pipline_2/jsonDatabaseEditor/python'
        if pipePath not in paths:
            sys.path.append(pipePath)
        import jsonDB
        j = jsonDB.AssetDB('test')

        # Assign database values to variables.
        for asset in j.assets:
            if asset.databaseID == assetID:
                self.assetHooks = asset.hooks
                self.assetAnchors = asset.anchors
                self.assetControls = asset.controls

        self.transforms = cmds.ls(tr = True)

    def cleanup(self):
        for t in self.transforms:
            if '_ctrlHook' in t:
                cmds.delete(t)
            elif '_ctrlAnchor' in t:
                cmds.delete(t)
            elif 'AssetMainCtrl' == t:
                atts = cmds.listAttr('AssetMainCtrl', userDefined = True)
                if atts != None:
                    for att in atts:
                        cmds.deleteAttr('AssetMainCtrl.' + str(att))

        self.transforms = cmds.ls(tr = True)

    def createGroups(self):

        if 'ALL' not in self.transforms:
            cmds.group(em = True, n = 'ALL')

        if 'CTRL' not in self.transforms:
            cmds.group(em = True, n = 'CTRL')
            cmds.parent('CTRL', 'ALL')

        if 'GEO' not in self.transforms:
            cmds.group(em = True, n = 'GEO')
            cmds.parent('GEO', 'ALL')

        if 'GUTS' not in self.transforms:
            cmds.group(em = True, n = 'GUTS')
            cmds.parent('GUTS', 'ALL')

        if 'AssetMainCtrl' not in self.transforms:
            cmds.circle(n = 'AssetMainCtrl')
            cmds.parent('AssetMainCtrl', 'CTRL')

    def createControls(self):
        print '\nIMPORTING CONTROLS...'
        existingCtrls = cmds.listAttr('AssetMainCtrl')

        typeDict = {}
        typeDict['int'] = 'long'
        typeDict['float'] = 'float'
        typeDict['enum'] = 'enum'
        typeDict['str'] = 'string'
        typeDict['bool'] = 'bool'
        typeDict['translate'] = 'double3'
        typeDict['rotate'] = 'double3'
        typeDict['scale'] = 'double3'
        typeDict['vector'] = 'double3'

        for c in self.assetControls:
            controlName = str(c.name.replace(' ', '_'))
            controlType = str(c.type)

            # delete control attribute if data type is mismatched with database
            if controlName in existingCtrls:
                 cType = str(cmds.getAttr('AssetMainCtrl.' + controlName, type = True))
                 if cType != typeDict[controlType]:
                     cmds.deleteAttr('AssetMainCtrl', at = controlName)

            # create control attribute if missing
            if controlName not in existingCtrls:
                cmds.select('AssetMainCtrl')

                if controlType in ['str', 'unicode']:
                    cmds.addAttr(longName = controlName, dt = 'string', hidden = False, keyable = True)
                    print '     ADDED: ' + controlName + '    ' + controlType

                elif controlType == 'enum':
                    line = ''
                    for v in c.value:
                        line = line + v + ':'
                    cmds.addAttr(longName = controlName, at = 'enum', enumName = line[:-1], hidden = False, keyable = True)
                    print '     ADDED: ' + controlName + '    ' + controlType

                elif controlType in ['translate', 'rotate', 'scale'] and controlName in ['translate', 'rotate', 'scale']:
                    print '     SKIPPED: ' + controlName + '      ' + controlType

                elif controlType in ['translate', 'rotate', 'scale']:
                    cmds.addAttr(longName = controlName + 'X', at = 'double', hidden = False, keyable = True)
                    cmds.addAttr(longName = controlName + 'Y', at = 'double', hidden = False, keyable = True)
                    cmds.addAttr(longName = controlName + 'Z', at = 'double', hidden = False, keyable = True)

                elif controlType == 'shader':
                    print '     SKIPPED: ' + controlName + '      ' + controlType

                else:
                    cmds.addAttr(longName = controlName, at = typeDict[controlType], hidden = False, keyable = True)
                    print '     ADDED: ' + controlName + '    ' + controlType

            # set control values
                if controlType == 'enum':
                    print '         VALUE NOT SET.'
                elif controlType == 'shader':
                    print '         VALUE NOT SET.'
                elif controlType in ['translate', 'rotate', 'scale']:
                    cmds.setAttr('AssetMainCtrl.' + controlName + 'X', c.value[0])
                    cmds.setAttr('AssetMainCtrl.' + controlName + 'Y', c.value[1])
                    cmds.setAttr('AssetMainCtrl.' + controlName + 'Z', c.value[2])
                elif controlType == 'str':
                    cmds.setAttr('AssetMainCtrl.' + controlName, str(c.value), type = 'string')
                else:
                    cmds.setAttr('AssetMainCtrl.' + controlName, c.value)
                    print '         VALUE SET: ' + str(c.value)

    def createHooks(self):
        print '\nIMPORTING HOOKS...'

        for hook in self.assetHooks:
            hookName = str(hook)
            prefAnchor = str(self.assetHooks[hook])

            # create ctrlHook locator if it does not exist
            if hookName not in self.transforms:
                cmds.spaceLocator(name = hookName, position = [0,0,0])
                print '     CREATED: ' + hookName
                cmds.parent(hookName, 'AssetMainCtrl')

            # create 'preferredAnchor' attribute if it does not exist on the ctrlhook
            hookAtts = cmds.listAttr(hookName)
            if 'preferredAnchor' not in hookAtts:
                cmds.select(hookName)
                cmds.addAttr(longName = 'preferredAnchor', dt = 'string')
                print '         PREFERRED ANCHOR ATTRIBUTE CREATED...'

            # set the value of 'preferredAnchor'
            cmds.setAttr(hookName + '.preferredAnchor', prefAnchor, type = 'string')
            print '         PREFERRED ANCHOR: ' + prefAnchor

    def constrainSingleHooks(self):
        if len(self.assetHooks) == 1:

            for hook in self.assetHooks:
                childObj = str(hook)

            # Set object to parent to. If only GEO has only 1 child, that is the parent. Else, GEO is parent.
            geoChildren = cmds.listRelatives('GEO', children = True)
            if len(geoChildren) == 1:
                parentObj = str(geoChildren[0])
            else:
                parentObj = 'GEO'

            # Create the Parent constraint
            cmds.parentConstraint(childObj, parentObj, mo = True)

            # Move the new parent constraint to the GUTS group
            childList = cmds.listRelatives(parentObj, children=True)

            for child in childList:
                if 'parentConstraint' in child:
                    cmds.parent(child, 'GUTS')

    def createAnchors(self):
        print '\nIMPORTING ANCHORS...'

        for anchor in self.assetAnchors:
            if anchor not in self.transforms:
                cmds.spaceLocator(name = anchor, position = [0,0,0])
                cmds.parent(anchor, 'GUTS')
                print '     CREATED: ' + anchor

    def ask(self):
        confirmMessage = 'Are you sure you want to add the following objects to the current scene?\n'
        confirmMessage = confirmMessage + '\nAttribute connections will be broken for custom controls.\n'
        confirmMessage = confirmMessage + '\nExisting ctrlHooks and ctrlAnchors will be deleted.\n'

        confirmMessage = confirmMessage + '\n\nNEW HOOKS:'
        for h in self.assetHooks:
            confirmMessage = confirmMessage + '\n       ' + str(h) + '   PREF ANCHOR: ' + str(self.assetHooks[h])

        confirmMessage = confirmMessage + '\n\nNEW ANCHORS:'
        for a in self.assetAnchors:
            confirmMessage = confirmMessage + '\n       ' + str(a)

        confirmMessage = confirmMessage + '\n\nNEW CONTROLS:'
        for c in self.assetControls:
            confirmMessage = confirmMessage + '\n       ' + str(c.name) + '       TYPE: ' + str(c.type)

        askContinue = cmds.confirmDialog( title='Pull from Database', message=confirmMessage, button=['Yes','No'], defaultButton='Yes', cancelButton='No', dismissString='No' )

        if askContinue == 'Yes':
            self.cleanup()
            self.createGroups()
            self.createControls()
            self.createHooks()
            self.constrainSingleHooks()
            self.createAnchors()
            print 'PULL FROM DATABASE COMPLETE!'

#p = PullFromDatabase()
#p.setup()
#p.ask()
