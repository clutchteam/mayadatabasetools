# Python
import os, sys
import json

# There are 2 types of Scenes Switch and Parralel. 
## A switch scene has the fewest number of assets with render layers for each config
## A Paralel scene has one render layer and all the configs exist simultainously.

## The switch variable should be True or False. You can not have a scene that is both Switch and Parralel

def niceConfigName(config):
    niceName = config.split('/')[::-1][0].split('.')[0] + '_'
    return niceName
  
def readConfig(config):
    with open(config) as f:
        data = json.load(f)
    return data
      
def assetList(switch,configs):
    listedAssets = []
    assetsToLoad = []
    for config in configs:
        data = readConfig(config)
        assets = data['assets']
        for asset in assets:
            fullName = asset['fullName']
            if switch:
                if fullName not in listedAssets:
                    listedAssets.append(fullName)
                    dict ={'fullname':fullName, 'path':asset['path'] ,'id': asset['databaseID']}
                    assetsToLoad.append(dict)
            else:
                fullName = niceConfigName(config) + fullName
                dict ={'fullname':fullName, 'path':asset['path'] ,'id': asset['databaseID']}
                assetsToLoad.append(dict)
    return assetsToLoad         

def layerList(switch,configs):
    #if switch is True a layer name is appended for each asset#
    if switch == True:
        layers = []
        for config in configs:
            layerName = niceConfigName(config) 
            layers.append(layerName)
    #if switch is False there is only one Layer#   
    else:
        layers = ['Parallel_']
    return layers
    
def overideList(switch,configs,dataBase):
    cfgCtrls = []
    dbCtrls = []
    overrides = []
    absTypes = ['float','int','enum']
    shdrTypes = ['shader']
    cnctntTypes = ['enumCnctn']
    picks = ['enum','shader','enumCnctn']
    
    # get value from database 
    dbData = readConfig(dataBase)
    dbassets = dbData['assets']
    for dbasset in dbassets:
        dbctrls =  dbasset['controls']
        for dbctrl in dbctrls:
            if dbctrl['type'] == 'enum':
                dbvalue = dbctrl['pick']
            else:
                dbvalue = dbctrl['value']
            dict = {'asset':dbasset['name'],'ctrl':dbctrl['name'],'val':dbvalue}
            dbCtrls.append(dict)

    for config in configs:
        if switch == True:
            layerName = niceConfigName(config)
        else:
            layerName = 'Parallel_'
        
        configData = readConfig(config)
        assets = configData['assets']    
        
        for asset in assets:
            # Determine full name base on the type of scene #
            if switch:
                fullName = asset['fullName']
            else:
                fullName = niceConfigName(config) + asset['fullName']
            
            ### CONTROL OVERIDES ###
            # Create list of controls #
            ctrls =  asset['controls']
            for ctrl in ctrls:
                # chose weather to get "Value" or "Pick"
                if ctrl['type'] in picks:
                    value = ctrl['pick']
                else:
                    value = ctrl['value']
                #define what type of overide#
                if ctrl['type'] in shdrTypes:
                    ovrdType = 'shader'
                if ctrl['type'] in cnctntTypes:
                    ovrdType = 'connection'
                else:
                    ovrdType = 'absolute'
                
                ctrlDict = {'layerName':layerName,'fullname':fullName,'asset':asset['name'],'ctrl':ctrl['name'],'val':value,'ovrdType':ovrdType,'attType':ctrl['type']}
                
                ### Compare to Absolute overides to the database ###
                match = False
                if ovrdType == 'absolute':
                    for db in dbCtrls:
                        if ctrlDict['asset'] == db['asset'] and ctrlDict['ctrl'] == db['ctrl'] and ctrlDict['val'] == db['val']:
                            match = True
                if match == False:
                    overrides.append(ctrlDict)
            ### HOOK OVERIDES ###
            hooks = asset['hooks']
            for hook in hooks:
                hookDict = {'layerName':layerName,'fullname':fullName,'asset':asset['name'],'ctrl':hook,'val':hooks[hook],'ovrdType':'connection','attType':'none'}
                overrides.append(hookDict)
    return overrides
